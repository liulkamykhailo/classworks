// const objectName = {
//     // ім'я властивості/ключ/key
//     // myFirstName: "Kolya",
//     // surName: "Nesmiyanov",
// }

/////////////////////////////////////////////////////////////////////////////////////////
// const car = {
//     model: "Lexus",
//     year: "2020",
//     color: "black",
//     owner: "Kolya",
//     price: "10$",
// }
/////////////////////////////////////////////////////////////////////////////////////////
// Задача 1
// Створити об'єкт student, який матиме такі властивості: ім'я(name), прізвище(last name),
// коефіцієнт ліні (laziness) рівний 4 і коефіцієнт хитрості (trick) рівний 5. Вивести його в консоль.

// const student = {
//     name: "Misha",
//     lastName: "Liulka",
//     laziness: 4,
//     trick: 5,
// };
// console.log(student);
/////////////////////////////////////////////////////////////////////////////////////////
// Задача 2
// Створити об'єкт product з такими полями: name, full name, price, availability (чи є на складі)
// зі значенням false, та additional gift (подарунок до покупки) зі значенням null. Вивести його в консоль.

// const product = {
//     name: "Some product",
//     fullName: "HP cl1220",
//     price: 1,
//     availability: false,
//     additionalGift: null,
// };
// console.log(product);

/////////////////////////////////////////////////////////////////////////////////////////
// Задача 3
// Треба скопіювати об'єкт. Якщо коефіцієнт ліні більший або дорівнює 3 і при цьому менший або дорівнює 5,
// а коефіцієнт хитрості менше або дорівнює 4
// вивести в консоль повідомлення "Студент із середнім завзяттям"

// const student = {
//     name: "Misha",
//     lastName: "Liulka",
//     laziness: 4,
//     trick: 2,
// };
// console.log(student);

// if (student.laziness >= 3 && student.laziness <= 5 && student.trick <= 4) {
//     console.log("Студент не лінивий");
// };
/////////////////////////////////////////////////////////////////////////////////////////
// задача 4
// Створити об'єкт користувача, який має три властивості:
//     - Ім'я;
//     - Прізвище;
//     - Професія.
// А також одним методом sayHi, який виводить у консоль повідомлення "Привіт" .

// const user = {
//     name: "Ivan",
//     lastName: "Gorohov",
//     profession: "Developer",
//     sayHi() {
//         console.log("Привіт");
//     },
// };
// user.sayHi();
// console.log(user);
/////////////////////////////////////////////////////////////////////////////////////////
// Задача 5
// Update попередній об'єкт додавши введення імені та фамілії через prompt

// const user = {
//     name: "Ivan",
//     lastName: "Gorohov",
//     profession: "Developer",
//     sayHi() {
//         console.log("Привіт");
//     },
// };
// user.name = prompt('Введіть ваше імя');
// user.lastName = prompt('Введіть ваше прізвище');
// console.log(user);
/////////////////////////////////////////////////////////////////////////////////////////
// Задача 6
// UPDATE до 4 задачі: Додати зарплатню і зробити перевіркуб якщо з.п. 2000$, вивести в консоль - ти вже не джун

// const user = {
//     name: "Ivan",
//     lastName: "Gorohov",
//     profession: "Developer",
//     sayHi() {
//         console.log("Привіт");
//     },
// };
// user.name = prompt('Введіть ваше імя');
// user.lastName = prompt('Введіть ваше прізвище');
// user.salary = +prompt('Ваша ЗП')
// console.log(user);
// if (user.salary > 2000) {
//     console.log('Красава');
// }
/////////////////////////////////////////////////////////////////////////////////////////
//Задача 7:
//Ввести ім'я, прізвище, роль та бажану з.п. Якщо з.п. менше 500$ вивести в консоль:
//"Для джуна зарплатня на грані виживання", якщо з.п. більше 500 і менше 1500 - вивести: "Тобі вже пора йти на мідла"
//Якщо з.п. більше 1500 і до 4000: "Вітаємо - ви strong middle", і якщо з.п. більше 4000: "Вас хоче найняти google"

// const user = {
//     name: "Ivan",
//     lastName: "Gorohov",
//     profession: "Developer",
//     sayHi() {
//         console.log("Привіт");
//     },
// };

// user.name = prompt('Введіть ваше імя');
// user.lastName = prompt('Введіть ваше прізвище');
// user.salary = +prompt('Ваша ЗП')

// if (user.salary >= 500 && user.salary <= 1500) {
//     console.log('Тобі пора на мідла');
// } else if (user.salary > 1500 && user.salary <= 4000) {
//     console.log('Вітаємо ви стронг мідл');
// } else if (user.salary > 4000) {
//     console.log('Вас хоче найняти гугл');
// } else {
//     console.log('Для джуна зп на грані виживання');
// }

// console.log(user);

/////////////////////////////////////////////////////////////////////////////////////////
// Задача 8:
// Розширити функціонал об'єкта з попереднього завдання (4)
//     - Метод sayHi має вивести повідомлення: "Привіт. Мене звати ІМ'Я ПРІЗВИЩЕ."; `use template literal`
//     - Додати метод, який змінює значення вказаної властивості об'єкта.

const user = {
    name: "Ivan",
    lastName: "Gorohov",
    profession: "Developer",
    sayHi() {
        console.log(`Привіт. Мене звати ${this.name} ${this.lastName}`);
    },

    updateProperty: function (propertyName, propertyValue) {
        if (propertyName in this) {
            this[propertyName] = propertyValue;
        } else {
            throw new Error (`Property ${propertyName} doesn't exist`);
        }
    }
};
user.sayHi();
console.log(user);


user.updateProperty('name', 'Grisha');
console.log(user);
user.updateProperty('profession', "CEO");
console.log(user);
user.updateProperty('profession', 'Designer');
console.log(user);