//function declaration/////////////////////////////////////////////////////////
// function sayHello (x, y) {
//     alert(x+y)
// }
// sayHello([],3);

// function sayHello ( parameters ) {
//     alert("Hello Everybody")
// }
// sayHello ( arguments );

//function expression////////////////////////////////////////////////////////////////
// const sayHello = function (x, y) {
//     console.log(x + y);
// }
// sayHello(2, 3);


//pure function/////////////////////////////////////////////////////////////////
// let some
// let mod = 1

// function pure(a) {
//     console.log(a);
// }
// pure("pure")
// console.log(some);

// not-pure function//////////////////////////////////////////////////////////////////
// let some
// let mod = 1

// function notPure(a) {
//     some = a + mod
// }
// notPure("notPure")
// console.log(some);

////2 варіант нечистої функції//////
// let counter = (function () {
//     let initValue = 0
//     return function () {
//         initValue++
//         return initValue
//     }
// })()

// counter();
// counter();
// counter();
// counter();

//arrow function///////////////////////////////////////////////////////
//з простої функції перероблюєм на стрілочну///

// let f = () => 15;
// let f2 = function () { return 15; };
// alert(f() + ' ' + f2());

//проста   let f2 = function () { return 15; };
//стрілочна    let f = () => 15;

/////////////////////////////////////////////////////////////////////////////////////////////////
//1.Написати функцію суматор.
//2.Функція має два числові параметриб і повертає результат їх складання.

//Завданяня 1
//1.Написати функцію суматор.

// function sum(a, b) {
//     return a + b;
// }

// console.log(sum(2, 3));
// console.log(sum(6, 3));
// console.log(sum(9, 3));
// console.log(sum(11, 3));



//Завданяня 2
// Написати функціюб яка визначає кількість переданих аргументів і вертає їх кількість

// function countArguments() {
//     return arguments.length
// }
// console.log(countArguments("string", 123456, NaN));
// console.log(countArguments([], {}, undefined, Object));



// Завдання 3
// Написати функцію-лічильник count.
// Функцію має буде володіти двома параметрами:
//  - Перший - число, з якого необхідно почати рахунок;
//  - Другий - число, яким необхідно закінчити рахунок.
 
//  Якщо число, з якого починається рахунок, більше, ніж число,
//  яким він закінчується, вивести повідомлення:
//  "Помилка! Рахунок неможливий."
 
//  Якщо обидва цих числа однакові, вивести повідомлення:
//  "Помилка! Нічого рахувати."
 
//  На початку рахунку необхідно вивести повідомлення:
//  "Відлік розпочато.".
 
//  Кожен "крок" рахунку необхідно виводити в консоль.
//  Після проходження останнього "кроку" необхідно вивести повідомлення:
//  "Відлік завершено.".


// function count(startCount, finishCount) {
//     if (startCount > finishCount) {
//         return console.log("Помилка, розрахунок неможливий");
//     } else if (startCount === finishCount) {
//         return console.log("Помилка, нічого рахувати");
//     }
//     console.log("Відлік розпочато");

//     for (let i = startCount; i <= finishCount; i++){
//         console.log(i);
//     }
//     return console.log("Відлік завершено")
// }


// count(4, 10);
// count(-11, 30);
// count(5, 5);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Завдання 4

// Функцію-лічильник із попереднього завдання розширити додатковим функціоналом:
//  - Додати їй третій параметр, і виводити в консоль тільки числа, кратні значенню з цього параметра;
//  Генерувати помилку, якщо функція була викликана не з трьома аргументами;
//  Генерувати помилку, якщо будь-який з аргументів не є допустимим числом.


function countAdvenced(start, finish, logCriteria) {
    if (arguments.length !== 3) {
        throw new Error("Допустима кількість аргументів для виклику функції: 3.")
    }

    for (const argument of arguments) {
        if (typeof argument !== "number" || Number.isNaN(argument)) {
            console.log(typeof argument)
            throw new Error("Аргумент не є коректним числом")
        }
    }
    console.log("Відлік розпочатий");

    for (let step = start; step <= finish; step++){
        if (step % logCriteria === 0) {
            console.log(step);
        }
    }
    console.log("Відлік завершений");
}
console.log("Відлік завершений");

countAdvenced(-9, 4, 22);
countAdvenced(4, 22, 10);
countAdvenced(4, 22);
countAdvenced(-8, 16, 11);
