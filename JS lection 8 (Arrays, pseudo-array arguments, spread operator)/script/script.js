// let arr = ["Jora", "Sergey", "Vitya", "Tanya", "Anna"];
////////////////////////////////////////////////////
//Додати пункт
// arr.push("Anna");
////////////////////////////////////////////////////
// console.log(arr);
// arr.at(-1);
// console.log(arr);

// arr.pop();
// console.log(arr);

// arr.indexOf("Sergey");
// console.log(arr.indexOf("Sergey"));

/////////////////////////////////////////////////////////
//Видалити елемент
// console.log(arr);
// arr.splice(3, 1);
// console.log(arr);
////////////////////////////////////
//Метод slice
// console.log(arr);
// let newArr = arr.slice(1, 4);
// console.log(newArr);
//////////////////////////////////////////////////////////////////////////
// let arr = ["Jora", "Sergey", "Vitya", "Tanya", "Anna"];
// let surName = ["Melnuk", "Tkachuk","Buba", "Biba", "Boba"];

// let newArray = arr.concat(surName);
// console.log(newArray);
////////////////////////////////////////////
// let names = ["Jora", "Sergey", "Vitya", "Tanya", "Anna"];
// let surName = ["Melnuk", "Tkachuk", "Buba", "Biba", "Boba"];

// names.forEach((item, element) => {
//     console.log(element);
// });

// console.log(surName.indexOf("Biba"));
//////////////////////////////////////////////////////////
//find
// let names = [
//     { firstName: "Jora", age: 15, id: 1 },
//     { firstName: "Sergey", age: 25, id: 2 },
//     { firstName: "Vitya", age: 34, id: 3 },
//     { firstName: "Tanya", age: 23, id: 4 },
//     { firstName: "Anna", age: 34, id: 5 }
// ];
// let newName = names.find(obj => obj.id == 5);
// console.log(newName);
///////////////////////////////////////////////////////////////
//map
// let names = [
//     { firstName: "Jora", age: 15, id: 1 },
//     { firstName: "Sergey", age: 25, id: 2 },
//     { firstName: "Vitya", age: 34, id: 3 },
//     { firstName: "Tanya", age: 23, id: 4 },
//     { firstName: "Anna", age: 34, id: 5 }
// ];
// let newOldName = names.map(item => item.firstName.length);
// console.log(newOldName);
////////////////////////////////////////////////////////////
//Сортування
// let newArray = [1, 2, 15, 23, 777, 21, 22, 768, 77];

// newArray.sort((a, b) => b - a);
// console.log(newArray);
//////////////////////////////////////////////////////////////////////////
//split - розділяє
//join - обєднує

// let str = 'Lexus, Bobik, ZAZ, Suzuki';

// let arr = str.split(', ');
// console.log(arr);
//////////////////////////////////////////////////////////////////
//reduce

// let arraySomeNumbers = [1, 5, 8, 1, 10];
// let result = arraySomeNumbers.reduce((multiply, current) => multiply * current);
// console.log(result);

/////////////////////////////////////////////////////////////////////////////
//spread

// let array1 = [1, 2, 3];
// let array2 = [4, 5, 6];

// let array3 = [...array1, ...array2];
// console.log(array3);
///////////////////////////////////////////////////////////////
//Клонування масива

// const original = ["one", "two"];
// const newOriginal = original;

// console.log(original);
// console.log(newOriginal);

// newOriginal[1] = "three";

// console.log(original);
// console.log(newOriginal);
////////////////////////////////////////////////////////////////////
//Привести рядок до масива за допомогою спред оператора
//Розбиває кожен знак на окремий елемент

// const cars = 'Lexus, Bobik, ZAZ, Suzuki';

// const newCars = [...cars];

// console.log(newCars);
///////////////////////////////////////////////////////////////////////////
// Задача 1:
//Дано масив із днями тижня.
// Вивести в консоль усі дні тижня за допомогою:
// - Класичного циклу for;
// - Циклу for...of;
// - Спеціалізованого методу для перебору елементів масиву forEach.

// const days = [
//   'Monday',
//   'Tuesday',
//   'Wednesday',
//   'Thursday',
//   'Friday',
//   'Saturday',
//   'Sunday',
// ];

// //1.
// for (let i = 0; i < days.length; i++) {
//     console.log(days[i]);
// };
// //2.
// for (let day of days) {
//     console.log(day);
// };
// //3.
// days.forEach((element) => { console.log(element); });




///////////////////////////////////////////////////////////////////
// Задача2:
// Написати мульти-мовну програму-органайзер.
// Програма запитує у користувача якою мовою він бажає побачити список днів тижня.

// Після введення користувачем бажаної мови програма виводить у консоль дні тижня вказаною мовою.

// Доступні мови (локалі): ua, je, en.
// Якщо введена користувачем локаль не збігається з доступними - програма перепитує його доти, поки доступна локаль не буде введена.
// Умови:
// Рішення має бути коротким, лаконічним і універсальним;
// - Усіляко використовувати методи масиву;
// - Використання методу Object.keys() обов'язкове.


const days = {
  ua: [
    'Понеділок',
    'Вівторок',
    'Середа',
    'Четвер',
    'П’ятниця',
    'Субота',
    'Неділя',
  ],
  de: [
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag',
    'Sonntag',
  ],
  en: [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ],
};

const avaibleLanguages = Object.keys(days);