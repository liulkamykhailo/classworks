// document.querySelector('#button').addEventListener('click', () => console.log('Натиснення відбулось'))
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//У разі втрати фокусу на інпуті виводиться повідомлення

// let input = document.querySelector('input'); //знайшли по тегу інпут через пошук по документу querySelector
// input.addEventListener('focusout', () => alert(input.value));
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 1
// Додайте JavaScript до кнопки, щоб при натисканні на неї зникав
// < div id = "text">

// let button = document.querySelector('button');
// let div = document.querySelector('#text')
// function outDiv() {
//     div.style.display = 'none';
// }
// button.addEventListener('click', outDiv);
// //Варіант 2 (я не дописав)
// let button = document.querySelector('button');
// button.addEventListener('click', () => )
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Задача 2:
//Створити кнопку, яка ховається при натисканні.

// let btn = document.querySelector('#btn');
// btn.addEventListener('click', () => btn.style.display = 'none')

//Задача 3:
//Написати скрипт, який створить елемент button із текстом "Увійти"
// При кліці по кнопкі виводити alert із повідомленням: "Ласкаво просимо"

// let button = document.createElement("button");
// button.innerHTML = "Увійти";
// document.body.prepend(button);
// button.addEventListener('click', () => alert('Ласкаво просимо'));

// Задача 4:
// Поліпшити скрипт із попереднього завдання.
// При наведенні на кнопку вказівником миші, виводити alert з повідомленням:
// "При кліці по кнопці ви ввійдете в систему.".
// Повідомлення має виводитися один раз.

// Задача 5:
// Створити меню, яке відкривається/згортається при натисканні:

// let btn = document.createElement('button');
// btn.innerText = 'Click me';
// document.body.appendChild(btn);
// let ul = document.querySelector('ul');
// ul.style.visibility = 'visible';
// btn.addEventListener('click', () => ul.style.visibility = 'hidden');

// Задача 6:
// Створіть список, елементи якого можна вибирати, як у файл-менеджерах
// Клік на елементі списку виділяє тільки цей елемент (додає клас .selected), а всі інші знімають виділення
// Якщо клік зроблено з натиснутою клавішею Ctrl (Cmd для Mac), то виділення перемикається на цьому елементі, але інші елементи не модифікуються.

let div = document.createElement("div")
let div2 = document.createElement("div")

div.style.width = "100px"
div.style.height = "100px"
div.style.background = "black"
div2.style.width = "100px"
div2.style.height = "100px"
div2.style.background = "black"

document.body.append(div)
document.body.append(div2)

div2.addEventListener("mouseover", () => div.style.background = "red")
div2.addEventListener("mouseout", () => div.style.background = "black")
div.addEventListener("mouseover", () => div2.style.background = "green")
div.addEventListener("mouseout", () => div2.style.background="black")

// Задача 7: 
// При наведенні на блок 1 робити блок 2 зеленим кольором
// А при наведенні на блок 2 робити блок 1 червоним кольором

// Задача 8:
// При натисканні на кнопку Validate відображати слово VALID зеленим кольром, якщо значення проходить валідацію слово INVALID червоним кольором, якщо значення не проходить валідацію
 
//  Правила валідації значення:
//  - значення не пусте
//  - повинно містити щонайменше 5 символів
//  - не повинно містити пробілів (indexof " ", trim() )
//  Елемент з cловом створити за допомогою js

// Задача 9:
// Створити елемент h1 із текстом "Ласкаво просимо!".
// Під елементом h1 додати елемент button з текстом "Розфарбувати".
// При кліці по кнопці змінювати колір кожної літери елемента h1 на випадковий.

//Задача 10:
//Поліпшити скрипт із попереднього завдання. При кожному русі курсору по сторінці змінювати колір кожної літери елемента h1 на випадковий.

//Задача 11: 
//Початкове значення лічильника 0 При натисканні на + збільшувати лічильник на 1 При натисканні на - зменшувати лічильник на 1 
//не давати можливості задавати лічильник менше 0