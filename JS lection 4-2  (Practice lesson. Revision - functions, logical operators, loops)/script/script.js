//Написати функцію лічильник increment.
//Перший виклик функції має повернути 0.
//Кожен новий виклик функції повинен повертати число, на 1 більше, ніж попереднє.

//1 варіант

// function createIncrement() {
//     let num = 0;
//     return function () {
//         return num++;
//     }
// }
// let increment2 = createIncrement();
// console.log(increment2());
// console.log(increment2());
// console.log(increment2());
// console.log(increment2());

//2 варіант те саме

// let num = 0;

// function createIncrement() {
//     return num++;
// }
// console.log(createIncrement());
// console.log(createIncrement());
// console.log(createIncrement());
// console.log(createIncrement());
////////////////////////////////////////////////////////////////////////////
//Написати функцію лічильник increment.
//Перший виклик функції має повернути 0.
//Кожен новий виклик функції повинен повертати число, на 1 більше, ніж попереднє.

//Кожен новий виклик батьківської функції createIncrement буде повертати нову 'дочірню' функцію-лічильник.
//Кожна нова дочірня функція повинна починати відлік з 0.


// function createIncrement() {
//     let accum = 0;
    
//     return function () {
//         return accum++;
//     }
// }
// let createIncrement1 = createIncrement();
// let createIncrement2 = createIncrement();

// console.log('increment1:', createIncrement1());
// console.log('increment1:', createIncrement1());
// console.log('increment1:', createIncrement1());
// console.log('increment1:', createIncrement1());

// console.log('increment2:', createIncrement2());
// console.log('increment2:', createIncrement2());
// console.log('increment2:', createIncrement2());
// console.log('increment2:', createIncrement2());
// console.log('increment2:', createIncrement2());

//////////////////////////////////////////////////////////////////////////////////////////////////////
//Написати функцію суматор усіх своїх параметрів.
//Генерувати помилку, якщо:
// - Хоч один із параметрів не є допустимим числом (у помилці вказати порядковий номер аргументу)
// - Якщо функція була викликана менше, ніж із двома аргументами.

// POC (proof of concept) (Уточнення розрахунок послідовності робимо зі сторони користувача чи розробника
// 2. куда веде кнопка 3. картинка із яблуком клікабельна, якщо так то на яку ссилку воно веде)...ЦЕ ПРОСТО ПРИКЛАД ПИТАНЬ


// function sumAll() {
//     let accum = 0;
//     if (arguments.length < 2) {
//         throw new Error('Функцію необхідно викликати хоча б із двома аргументами');
//     }
//     let argCount = 1;
//         for (argument of arguments) {
//             if (typeof argument !== 'number' || Number.isNaN(argument)) {
//                throw new Error (`Аргумент ${argCount} являється некоректним числом`)
//             }
//             argCount += 1;
//             accum += argument;
//     }
//     return accum;
// }


// console.log(sumAll(1, 2, 6, 8, 1.3));
// console.log(sumAll(1, 2, 6, 'some text', 1.3));
// console.log(sumAll(1));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//1.Написати функцію суматор суматор 5 чисел(число задаємо самі через змінну). Робити через змінні і виводити в консоль.

//2.Вивести в консоль з допомогою функції власне імя та фамілія(робити через змінні і виводити в консоль)

//3.Дано 100 чисел додати тільки парні числа

//4.Дізнатися суму перших 100 чисел, які діляться на 4

//5.Написати фунцію, яка визначає кількість переданих аргументів і повертає їх кількість

//6.Користувач має ввести 2 значення і потрібно повернути суму цих значень(перевірку на число не робимо)

//7.Update додати перевірку на число при введенні користувача

//8.Є 10 чисел і потрібно визначити їх середнє число і вивести в alert

//9.Запитати користувача ввести якесь слово і визничити довжину цього слова і вивести в консоль цю довжину

//10.Update: до вище введеного слова додати умову: якщо довжина менше 10 букв потрібно попросити користувача щоб ввів слово в якому більше 10 символів.

//11.Є ваше ім'я та фамілія які відразу виводяться в консолі, попросити змінити імя на інше в користувача
//і вивести змінене ім'я в іншій консольці.



//12.Вивести перші 500 чисел в консоль (використати чим меньше коду(Колі відправити самий коротший спосіб))



//1.
// function sum (a, b, c, d, e) {
//     // let result = a + b + c + d + e;
//     // return result;
// }
// console.log(sum(1, 2, 3, 4, 5));

//2.

// function name() {
//     let firstName = prompt('Ваше імя');
//     let surName = prompt('Ваше прізвище');

//     let fullname = `${firstName} ` + `${surName}`;
//     return fullname;
    
// }
// console.log(name());

//3.
// function summNumber() {
//     let numb = 0;
//     for (let i = 0; i < 100; i++) {
//         if (i % 2 === 0) {
//             numb += i;
//         }
        
//     }
//     return numb;
// }
// console.log(summNumber());

//4.

// function summNumber() {
//     let num = 0;
//     for (let i = 1; i <= 100; i++){
//         if (i % 4 === 0) {
//             num += i;
//             console.log(i);
//         }
//     }
//     return num;
// }
// console.log(summNumber());

//5.
function someArguments() {
    
}

//12.
// let i = 1;
// for (; i <= 500;){
//     console.log(i++);
// }
// //або
// for (let i = 1; i <= 500; i++) {
//     console.log(i);    
// }