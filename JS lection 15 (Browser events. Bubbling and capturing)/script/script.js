////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 1

//відміняємо стандартну дію браузера  event.preventDefault();
// let myAnchor = document.getElementById("myAnchor");
// myAnchor.addEventListener("click", function (event) {
//     console.log("Ми відмінили стандартну дію браузеру");
//     event.preventDefault();
// }, false);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 2

//визначити на яку кнопку був здійснений клік
// const buttons = document.getElementById("buttons");
// buttons.addEventListener("click", (e) => {
//     console.log(e.target);
// })

//варіант 2
// const buttons = document.getElementsByClassName("classBtn");
// for (const button of buttons) {
//     button.addEventListener("click", () => console.log("Clicked"));
// }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 3

//Дивимось скільки кліків було по кнопці проведено
// let input = document.querySelectorAll('input');
// console.log(input);

// function getClick(e) {
//     if (e.target.value) {
//         e.target.value++;
//         console.log(e.target.value);
//     }
// }
// document.body.addEventListener("click", getClick)
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 4

//Є кнопка, коли клацаєш на кнопку зявляється форма або зникає
// let button = document.getElementById("button");
// let form = document.querySelector('[data-form="dataForm"]')
// // console.log(form);
// function clickButton() {
//     form.classList.toggle('form')
// }
// button.addEventListener('click', clickButton);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 5

// Необхідно "оживити" навігаційне меню за допомогою JavaScript.
// При кліці на елемент меню додавати до нього CSS-клас .active.
// Якщо такий клас уже існує на іншому елементі меню, необхідно з того, попереднього елемента CSS-клас .active зняти.
// У кожен елемент меню - це посилання, що веде на google.
// За допомогою JavaScript необхідно запобігти переходу за всіма посиланнями на цей зовнішній ресурс.
// Умови:
// - У реалізації обов'язково використовувати прийом делегування подій (на весь скрипт слухач має бути один).

// let ul = document.getElementById("list")
// ul.addEventListener("click", changeClass)

// function changeClass(e) {
//     e.preventDefault()
//     e.target.classList.toggle('active')
// }
// console.log();

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 6

// Напишіть код, який під час кліка на будь-який div всередині root виводитиме в консоль його id.

// let div = document.getElementById('root');
// div.addEventListener("click", (e) => {
//     console.log(e.target.id);
// })



//варіант 2 коли працює тальки на діви
// в строчці if (e.target.tagName === 'DIV') ім'я тегу 'DIV' обов'язково потрібно писати з великої букви
// const root = document.querySelector('#root')

// root.addEventListener('click', (e) => {
//     if (e.target.tagName === 'DIV') {
//         console.log(e.target.id);
//     }
// })
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 7

// В нас є готове навігаційне меню. Нам потрібно при кліку додавати фон тому елементу, на який було клікнуто.
// В кожного елемента свій колір фону, якщо змінити послідовність елементів колір в них має змінюватись як перший раз.
// (Уточнення: Фон не має бути прикріплений до послідовності елементів)

ul.addEventListener('click', changeColor);

function changeColor(e) {
    let link = e.target
    if (e.target.closest('a')) {
        e.preventDefault();
        link.style.backgroundColor ? link.removeAttribute('style') : link.style.backgroundColor = link.datast.color;
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 8

// Є Умова
// <ul>
//   <li>item <b>don`t do anything</b> item</li>
//   <li>item <b>don`t do anything</b> item</li>
//   <li>item <b>don`t do anything</b> item</li>
//   <li>item <b>don`t do anything</b> item</li>
//   <li>item <b>don`t do anything</b> item</li>
// </ul>
// При кліку на <li> добавити знак оклику ! в кінець, а при кліку на <b> нічого не робити.



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 9

//             <form onclick="handlerForm(event)">
//               FORM
//               <div onclick="handlerDiv(event)">
//                 DIV
//                 <p onclick="handlerP(event)">P</p>
//               </div>
//             </form>
// При кліку на форму в консольці виводити форму, а в алерті виводити тільки форму
// При  кліку на div в консольці виводити дів та форму, а в алерті виводити тільки div
// При кліку на р в консольці виводити р, div, form, а в алерті виводити тільки p


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 10

// Є 3 картинки, при кліку на картинку робити дублікат ції картинки і добавляти його в кінець списку
