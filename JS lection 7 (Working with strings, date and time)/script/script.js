//////////////////////////////////////////////////////////////////////////
//Написати функцію суматор двох чисел аби результат виводився в зворотні лапки

// function summa(a, b) {
//     return a + b;
// }
// alert(`2 + 8 = ${summa(2,8)}`)

///////////////////////////////////////////////////////////////////////////////////////

// let treasure = "Tresure:\n * Generator\n * Family\n* Bazooka a few some words";
// alert(treasure)

//////////////////////////////////////////////////////////////////////////
// alert("Bayer\u00A9")
/////////////////////////////////////////////////////////////////////
//Довжина рядка
// alert(`some Text`.length)
/////////////////////////////////////////////////////////////////////////
// let someVariable = `My personal computer`;
// console.log(someVariable[0]);
// console.log(someVariable.charAt(0));

// console.log(someVariable[60]);
// console.log(someVariable.charAt(60));
/////////////////////////////////////////////////////////////////////////
//Заміна букв

// let approveText = 'Ok'; //Ok=>NO
// console.log(approveText);

// let cancelText = 'N' + approveText[0].toLowerCase();
// console.log(cancelText);

//////////////////////////////////////////////////////////////////////////////
//Шукає символи Іа

// let str = 'Ослик Іа-Іа подивився на віадук';

// let target = 'Іа';
// let pos = 0;
// while (true) {
//     let foundPos = str.indexOf(target, pos);
//     if (foundPos == -1) break;

//     alert(`Знайдено тут: ${foundPos}`);
//     pos = foundPos + 1;
// }

//////////////////////////////////////////////////////////////////////////////////////////
//Шукає слово some в тексті

// alert("Find some text".includes("some", 0));

/////////////////////////////////////////////////////////////////////////////
//Витягнути текст за допомогою слайса

// let text = "findSomeText";

// console.log(text.slice(8));
//////////////////////////////////////////////////////////////
//Substring

// let text = "findSomeText";
// console.log(text.substring(0, 4));

////////////////////////////////////////////////////////////////////////////////////////////
//1. Дано рядок 'js'. Зробіть із нього рядок 'JS'.
//2. Дано рядок 'JS'. Зробіть із нього рядок 'js'.
//3. Дано рядок 'я вчу javascript!'. Знайдіть кількість символів у цьому рядку.
//4. Дано рядок 'я вчу javascript!'. Виріжте з нього слово 'вчу' і слово 'javascript'
// трьома різними способами (через substr, substring, slice).
//5. Дано рядок 'я вчу javascript!'. Знайдіть позицію підрядка 'вчу'.
//6. Дано рядок 'після випуску курсів в danIt я буду гуру front-end' Кожне слово потрібно
// зробити із великої літери
//7. update: із завдання вище потрібно з допомогою символів кожне слово зробити із нового рядка
//8. update: знайти довжину рядка, який вказано вище
//9. update: замінити слово гуру на Senior.


//1.
// let text = "js"
// console.log(text);
// console.log(text.toUpperCase());
//2.
// let text = "JS";
// console.log(text.toLowerCase());
//3.
// let text = "я вчу javascript!";
// console.log(text.length);
//4.
// let text = "я вчу javascript!";
// console.log(text.slice(0, 1) + text.slice(-1));
// console.log(text.substr(0, 1) + text.substr(-1));
// console.log(text.substring(0, 1) + text.substring(16));

//5.
// let text = "я вчу javascript!";

// let target = "вчу";
// let position = 0;

// while (true) {
//     let foundPosition = text.indexOf(target, position);
//     if (foundPosition == -1) break;

//     console.log(`${foundPosition}`);
//     position = foundPosition + 1;
// }

//6.(Біля першого слова вставляє пробіл)
// let text = 'після випуску курсів в danIt я буду гуру front-end';
// let upperText = text.split(' ');
// console.log(upperText);
// let result = '';
// for (let char of upperText) {
//     char = char[0].toUpperCase() + char.slice(1);
//     result += ' ' + char;
// }
// console.log(result);

//7.
// let text = 'після випуску курсів в danIt я буду гуру front-end';
// let newText = text.split(' ');
// console.log(newText.join('\n'));

//9.
// let text = 'після випуску курсів в danIt я буду гуру front-end';
// let newText = text.replace('гуру', 'Senior');
// console.log(newText);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Дата лічильник зворотнього відліку

// let countDownDate = new Date("Jan 1, 23, 00:00:00").getTime();
// //Оновлюємо відлік кожну секунду.

// let x = setInterval(function () {
//     //Отримати сьогоднішню дату та час
//     let now = new Date().getTime();
//     //Знайти скільки часу залишилось
//     let distance = countDownDate - now;
//     // console.log(distance);
//     //В дужках нижче (години * хвилини * сукунди * мілісекунди)
//     let days = Math.floor(distance / (24 * 60 * 60 * 1000));
//     // console.log(days);
//     let hours = Math.floor((distance % (60 * 60 * 1000*24))/(1000*60*60));
//     // console.log(hours);
//     let minutes = Math.floor((distance % (60 * 60* 1000)) / (1000*60));
//     // console.log(minutes);
//     let seconds = Math.floor((distance % (1000 * 60)) / 1000);
//     // console.log(seconds);
//     //Вивести результат в елементі в дом-дерево
//     document.getElementById("demo").innerHTML = days + "d " + hours + "h " + minutes + "m " + seconds + "s";
// //Зупинити відлік якщо час вийшов
//     if (distance < 0) {
//         clearInterval(x);
//         document.getElementById("demo").innerHTML = "Time is out"
//     }
// })