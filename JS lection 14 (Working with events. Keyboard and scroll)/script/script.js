/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Створити елемент h1 з текстом "Натисніть будь-яку клавішу.".
// При натисканні будь-якої клавіші клавіатури змінювати текст елемента h1 на:
// "Натиснута клавіша: ІМ'Я_КЛАВІШІ".

// let element = document.createElement('h1');
// element.innerHTML = "Натисніть будь-яку клавішу.";
// document.body.append(element);
// document.addEventListener('keydown', e => element.innerText = `Начтиснута клавіша ${e.code}`);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Задача 2:
// В консолі виводити назву клавіши коли нажимаєш та віджимаєш будь-яку.
// Приклад: Кнопка нажата. Key code значення:

// document.addEventListener('keydown', e => console.log(`Кнопка нажата. Key code значення: ${e.code}`));
// document.addEventListener('keyup', e => console.log(`Кнопка віджата. Key code значення: ${e.code}`));

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Задача 3:
//Створити 3 блоки, коли перший блок активний відбувається підрахунок скільки разів було нажато будь-яку кнопку.
//При натисканні на інших блоках нічого не відбувається

// let block1 = document.createElement('div');
// let block2 = document.createElement('div');
// let block3 = document.createElement('div');

// block1.style.cssText = `
// width: 100px;
// height: 100px;
// background-color: green;
// margin-bottom: 15px;`
// block2.style.cssText = `
// width: 100px;
// height: 100px;
// background-color: blue;
// margin-bottom: 15px;`
// block3.style.cssText = `
// width: 100px;
// height: 100px;
// background-color: blue;
// margin-bottom: 15px;`

// document.body.append(block1, block2, block3);
// block1.tabIndex = 1;
// block2.tabIndex = 2;
// block3.tabIndex = 3;//tabIndex задає послідовність виділення при натисканні Табу

// let count = 0;
// block1.addEventListener('keydown', () => {
//     count += 1;
//     console.log(count)
// });

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 4:
// Рахувати кількість натискань на пробіл, Enter,  та Backspace клавіші  Відображати результат на сторінці

let enterCount = 0;
let backSpaceCount = 0;
let spaceCount = 0;

document.addEventListener('keydown', () => {
    switch (event.code) {
        case 'Space':
            spaceCount++
            let first = document.getElementById('first')
            first.innerText = `Space pressed ${spaceCount} times`
            break;
            case 'Enter':
            enterCount++
            let second = document.getElementById('second')
            second.innerText = `Enter pressed ${enterCount} times`
            break;
            case 'BackSpace':
            backSpaceCount++
            let third = document.getElementById('third')
            third.innerText = `Back space pressed ${backSpaceCount} times`
            break;
    }
})


//Задача 5:
// При натисканні shift та "+"" одночасно збільшувати  шрифт сторінки на 1px а при shift та "-" - зменшувати на 1px  Максимальний розмір шрифту - 30px, мінімальний - 10px

//Задача 6: 
// При натисканні на enter в полі вводу додавати його значення, якщо воно не пусте, до списку задач та очищувати поле вводу.
//  При натисканні Ctrl + D на сторінці видаляти останню додану задачу
// Додати можливість очищувати весь список
// Запустити очищення можна двома способами:
// - при кліці на кнопку Clear all
// - при натисканні на Alt + Shift + Backspace

//  При очищенні необхідно запитувати у користувача підтвердження (показувати модальне вікно з вибором Ok / Cancel)
//  Якщо користувач підвердить видалення, то очищувати список, інакше нічого не робити зі списком

// {/* <input type="text" placeholder="Enter your task" id="new-task">
// <ul class="tasks-list">
// </ul>
// <button id="clear">Clear all</button> */}
// <p>Enter pressed: <span id="enter-counter">0</span></p>
//     <p>Space pressed: <span id="space-counter">0</span></p>
//     <p>Backspace pressed: <span id="backspace-counter">0</span></p>
// P.S. 
///в html вручну створити h1, p, h2, p, h3, p (не через js)
// P. S. - 5
///в html вручну створити h1, p, h2, p, h3, p (не через js)


