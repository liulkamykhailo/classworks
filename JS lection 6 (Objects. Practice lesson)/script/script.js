/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Копіювання об'єкта через Object.assign


// const car1 = {
//     carName: 'Lexus',
//     model: 'Rx777'
// };

// const car2 = Object.assign({}, car1);
// console.log(car1); // Lexus, Rx777
// console.log(car2); // Lexus, Rx777
// car1.model = "Bobik";
// console.log(car1.model); //Bobik
// console.log(car2.model); //Rx777
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Копіювання об'єкта через for...in


// const car1 = {
//     carName: 'Lexus',
//     model: 'Rx777'
// };

// const car2 = {};

// for (let key in car1) {
//     car2[key] = car1[key]
// }

// console.log(car1);
// console.log(car2);

// car2.model = "Bobik";

// console.log(car1.model);
// console.log(car2.model);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 1

// Створити із студентами аудиторія об'єкт, всередині цього класу має бути 3 студентів
// в яких буде ще один об'єкт і в цих об'єктах були задані поля: key(html, css, js)
// ш в кожному цьому кейсі були задано відсотків наскільки вони розуміються на тих
// технологіях
// console.log("Вивести все");
// console.log("Вивести кожного студента");
// console.log("Вивести значення серед технологій із кількістю відсотків");

// const createAuditory = {
//     student1: {
//         html: '80%',
//         css: '70%',
//         js: '90%',
//     },
//     student2: {
//         html: '95%',
//         css: '95%',
//         js: '95%',
//     },
//     student3: {
//         html: '100%',
//         css: '80%',
//         js: '90%',
//     },
// };
// console.log(createAuditory);
// console.log(createAuditory.student1);
// console.log(createAuditory.student2);
// console.log(createAuditory.student3);
// console.log(createAuditory.student1.html);
// console.log(createAuditory.student2.css);
// console.log(createAuditory.student3.js);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 2

// - Додати метод, який додає об'єкту нову властивіть із зазначеним значенням.

// Додатково
// Метод має бути "розумним" - він генерує помилку при створенні нової властивості
// Властивіть із таким іменем вже існує.

// const user = {
//     name: "Petro",
//     lastName: "Bilkin",
//     profession: "Manager",
//     newWalue: function (propertyName, propertyValue) {
//         if (propertyName in this) {
//             throw new Error ("Властивість з такою назвою вже існує");
//         } else {
//             this[propertyName] = propertyValue;
//         }
//     }
// };
// console.log(user);
// user.newWalue('age', 27);
// user.newWalue('sex', "male");
// console.log(user);
// user.newWalue('name', "Mykola");

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Задача 3

// За допомогою циклу for...in вивести в консоль усі властивості
// першого рівня об'єкта у форматі "ключ-значення".

// Додатково
// Поліпшити цикл так, щоб він умів виводити властивості об'єкта другого рівня вкладеності.

// Дано
// const user = {
//     firstName: "Walter",
//     LastName: "White",
//     job: "Programmer",
//     pets: {
//         cat: "Kitty",
//         dog: "Doggy",
//     },
// };
//варіант 1

// for (let key in user) {
//     if (typeof user[key] === 'object' && user[key] !== null) {
//         for (let key2 in user[key]) {
//             console.log(`${key2} - ${user[key][key2]}`);
//         }
//     } else {
//         console.log(`${key} - ${user[key]}`);
//     }
// };

//варіант 2
// for (const key in user) {
//     if (user[key] instanceof Object) {
//         Object.keys(user[key]).forEach(item => console.log(item, '-', user[key][item]));
//     } else {
//         console.log(key, '-', user[key]);
//     }
// }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Завдання 4.

// Написати функцію-помічник для ресторану.

// Функція має два параметри:
// - Розмір замовлення (small, medium, large);
// Тип обіду (breakfast, lunch, dinner).
// Функція повертає об'єкт із двома полями:
// - totalPrice - загальна сума страви з урахуванням її розміру та типу;
// - totalCalories - кількість калорій, що міститься у страві з урахуванням її розміру та типу.

// Замітки:
// - Додаткові перевірки здійснювати не потрібно;
// - У рішенні використовувати референтний об'єкт із цінами та калоріями.

//Дано
// const priceList = {
//     sizes: {
//         small: {
//             price: 15,
//             calories: 250,
//         },
//         medium: {
//             price: 25,
//             calories: 340,
//         },
//         large: {
//             price: 35,
//             calories: 440,
//         },
//     },
//     types: {
//         breakfast: {
//             price: 4,
//             calories: 25,
//         },
//         lunch: {
//             price: 5,
//             calories: 5,
//         },
//         dinner: {
//             price: 10,
//             calories: 50,
//         },
//     },
    //варіант 1
    // calcTotals(size, type) {
    // return {
    //     totalPrice: this.sizes[size].price * this.types[type].price,
    //     totalCalories: this.sizes[size].calories * this.types[type].calories,
    //  }
    // }
// };
//варіант 2
// function getOrderPrice(size, type) {
//     let totalPrice = 0;
//     let totalCalories = 0;
  
//     totalPrice += priceList.sizes[size].price;
//     totalPrice += priceList.types[type].price;
  
//     totalCalories += priceList.sizes[size].calories;
//     totalCalories += priceList.types[type].calories;
  
//     return {
//       totalPrice,
//       totalCalories,
//     };
// }
//виклик в консоль з варіанта 2
//     console.log(getOrderPrice('medium', 'lunch'));
//   console.log(getOrderPrice('small', 'dinner'));

//виклик в консоль з варіанта 1
//в значення this.sizes[size].price те що знаходиться в квадратних дужках передається значення те що ми передаємо нижче в строчці
// console.log(priceList.calcTotals('small', 'breakfast'));
// console.log(priceList.calcTotals('large', 'lunch'));

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Завдання додатково
// Написати функцію-фабрику, яка повертає об'єкти користувачів.
// Об'єкт користувача має три властивості:
// - Ім'я;
// - Прізвище;
// - Професія.
// Функція-фабрика, у свою чергу, володіє трьома параметрами,
// які відображають вищеописані властивості об'єкта.
// Кожен параметр функції має значення за замовчуванням: null.

let user = {
    firstName: "Grisha",
    lastName: "Yablunozhko",
    profession: "manager",
}

// Завдання додатково 2
//  Створити об'єкт користувача, який має три властивості:
//  - Ім'я;
//  - Прізвище;
//  - Професія.

//  Умови:
//  - Усі властивості мають бути доступні тільки для запису;
//  - Розв'язати задачу двома способами.

//Завдання додатково 3
//  Написати імплементацію методу Object.freeze().

//  Метод повинен:
//  - Запобігати можливості додавання нових властивостей до об'єкта;
//  - Запобігати можливості видалення наявних властивостей об'єкта;
//  Запобігати можливості зміни дескрипторів властивостей об'єкта;
//  - Запобігати можливості зміни значення властивостей об'єкта.

//  Умови:
//  - Вбудованим методом Object.freeze() користуватися заборонено;
//  - Вбудованим методом Object.preventExtensions() користуватися можна.



const days = {
  ua: [
    'Понеділок',
    'Вівторок',
    'Середа',
    'Четвер',
    'П’ятниця',
    'Субота',
    'Неділя',
  ],
  de: [
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag',
    'Sonntag',
  ],
  en: [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
  ],
};
 
/* Вирішення */
const availableLanguages = Object.keys(days);
let locale = prompt(
  `Введіть бажану локалізацію. Доступні: ${availableLanguages}.`,
  'ua',
);
 
while (!availableLanguages.includes(locale)) {
  locale = prompt(
    `Ви ввели не існуючу локалізацію, спробуйте ще раз. Доступі: ${availableLanguages}.`,
    'ua',
  );
}
 
days[locale].forEach(day => {
  console.log(day);
});
