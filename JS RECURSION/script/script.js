
//Рекурсія light
// let number = 0;
// function muchNumber() {
//     number++;
//     console.log(number);
//     if (number === 200) {
//         return;
//     }
//     muchNumber()
// }
// muchNumber();


//Цикл
// function f2() {
//     let out = '';
//     let i = 1;
// }



//Рекурсія
// let out = '';
// let i = 1;
// function f3() {
//     i++;
//     out += i + ' ';
//     if (i >= 30) return;
//     f3();
// }
// f3();
// console.log(out);



//Програма повинна назбирати 200.
// function reandomInteger (min, max) {
//     let randome = min + Math.random() * (max + 1 - min);
//     return Math.floor(randome);
// }

// let summ = 0;
// function moneyRecursion() {
//     let m = reandomInteger(0, 20)
//     console.log('add', + m);
//     summ += m;
//     console.log('summa =', + summ);
//     if (summ >= 200) return;
//     moneyRecursion();
// }
// moneyRecursion();



//Треба вивести всіх parent
const users = {
    "boyko": {
        age: 25,
        parent: {
            "tarasova": {
                age: 45
            },
            "schpuchka": {
                age: 43,
                parent: {
                    "schpuchka-b": {
                        age: 88,
                        parent: {
                            "logvienko": {
                                parent: {
                                    "Koka": {}
                                }
                            }
                        }
                    },
                }
            }
        }
    },
    "borusov": {
        age: 56,
        parent: {
            "biluy": {
 
            },
            "bogdanyuk": {
                age: 45
            }
        }
    }
}


// function userParentRecursion(obj) {
//     if (obj.parent !== undefined) {
//         for (let key in obj.parent) {
//             console.log(key);
//             userParentRecursion(obj.parent[key])
//         }
//     }
// }
// userParentRecursion(users.boyko);

//Оновимо код, щоб виводило всіх парентс а не тільки boyko
function userParentRecursion(obj) {
    if (obj.parent !== undefined) {
        for (let key in obj.parent) {
            console.log(key);
            userParentRecursion(obj.parent[key])
        }
    }
}
for (let key in users) {
    userParentRecursion(users[key]);
}